﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.DataAccess;
using UserManagement.DataAccess.Contract;
using UserManagement.DataAccess.Method;
using UserManagement.Entity;

namespace UserManagement.Business
{
    public class BALUserManagement
    {
        IUserManagement iUserManagement = new DALUserManagement();

        public string SaveUser(EntityUser User)
        {
            return iUserManagement.SaveUser(User);
        }
    }
}
