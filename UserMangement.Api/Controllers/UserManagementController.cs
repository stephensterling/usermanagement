﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using UserManagement.Entity;

namespace UserMangement.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserManagementController : ControllerBase
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public UserManagementController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public JsonResult SaveUser([FromBody]EntityUser user)
        {
            ReturnStatus res = new ReturnStatus();
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            var filePath = contentRootPath + "\\Data\\UserData.txt";
            
            var jsonData = System.IO.File.ReadAllText(filePath);
            
            var userList = JsonConvert.DeserializeObject<List<EntityUser>>(jsonData)
                                  ?? new List<EntityUser>();

            userList.Add(user);
           
            jsonData = JsonConvert.SerializeObject(userList);
            System.IO.File.WriteAllText(filePath, jsonData);            
            
            return new JsonResult(res);
        }

        [HttpGet]
        public JsonResult GetUsers()
        {
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            var filePath = contentRootPath + "\\Data\\UserData.txt";

            var jsonData = System.IO.File.ReadAllText(filePath);

            var userList = JsonConvert.DeserializeObject<List<EntityUser>>(jsonData)
                                  ?? new List<EntityUser>();

            return new JsonResult(userList);
        }



    }
}