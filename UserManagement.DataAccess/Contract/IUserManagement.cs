﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Entity;

namespace UserManagement.DataAccess.Contract
{
    public interface IUserManagement
    {
        string SaveUser(EntityUser User);
    }
}
