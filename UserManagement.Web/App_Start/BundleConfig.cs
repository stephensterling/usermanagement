﻿using System.Web;
using System.Web.Optimization;
using System.Web.Optimization.React;

namespace UserManagement.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new BabelBundle("~/bundles/main").Include("~/Scripts/React/Dashboard.jsx",
                                                            "~/Scripts/React/UserManagementList.jsx",
                                                            "~/Scripts/React/UserManagementAdd.jsx"));

            


        }
    }
}
