﻿$(document).ready(function () {
    $("#myDeviceBtn").click(function () {
        var url = "http://localhost:12345/api/GetDeviceStatus";
        $.ajax({
            type: 'GET',
	    headers: {
                    'Access-Control-Allow-Origin': '*'
                },
            url: url,
            success: function (data) {
                $("#divContent").html(data);
            },
            failure: function (data) {
                alert("Failure: " + data);
            },
            error: function (data) {
                alert("Error: " + data);
            }
        });
    });
});


