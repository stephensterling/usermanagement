﻿class UserManagementAdd extends React.Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();

        const formData = {};
        for (const field in this.refs) {
            formData[field] = this.refs[field].value;
        }        
        
        axios({
            method: 'post',
            url: apiUrl + "UserManagement/SaveUser",
            data: formData
        })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });

    };


    render() {
        return (
            
            <form onSubmit={this.handleSubmit}>
            <div className="row">
                <div className="col-lg-12">
                    
                    <div className="row">
                        <div className="col-lg-3">
                            First Name
                        </div>
                        <div className="col-lg-3">
                                <input type="text" name="firstName" ref="firstName" />
                        </div>
                        <div className="col-lg-3">
                            Last Name
                        </div>
                        <div className="col-lg-3">
                                <input type="text" name="lastName" ref="lastName" />
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-lg-3">
                            Email
                        </div>
                        <div className="col-lg-3">
                                <input type="text" name="email" ref="email" />
                        </div>
                        <div className="col-lg-3">
                            Mobile Number
                        </div>
                        <div className="col-lg-3">
                                <input type="text" name="mobileNumber" ref="mobileNumber" />
                        </div>
                    </div>

                    <div className="row">
                            <div className="col-lg-12 pull-right">
                                <input type="submit" value="Submit" />                            
                                <input type="button" value="Cancel" />
                        </div>                        
                    </div>

                </div>
            </div>
            </form>
        );
    }
}
ReactDOM.render(<UserManagementAdd />, document.getElementById('divUserManagementAdd'));