﻿class UserManagementList extends React.Component {

    constructor() {
        super();
        this.state = {
            userList: []
        }  

    } 

    
    componentDidMount() {

        axios.get(apiUrl + "UserManagement/GetUsers").then(response => {
            
            this.setState({
                userList: response.data
            });
        });
        
    }  


    render() {

        return (

            <table width="100%">
                <thead>

                    <tr>
                        <td>First Name</td>
                        <td>Last Name</td>
                        <td>Email</td>
                        <td>Mobile Number</td>
                    </tr>

                </thead>
                <tbody>
                    {this.state.userList.map(user =>
                        <tr>
                            <td>{user.firstName}</td>
                            <td>{user.lastName}</td>
                            <td>{user.email}</td>
                            <td>{user.mobileNumber}</td>
                        </tr>
                    )}

                </tbody>

            </table>

        );
    }
}
ReactDOM.render(<UserManagementList />, document.getElementById('divUserManagementList'));